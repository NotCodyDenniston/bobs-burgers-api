package com.example.bobsburgersapi.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.bobsburgersapi.model.FriendRepo
import com.example.bobsburgersapi.model.remote.NetworkResponses
import com.example.bobsburgersapi.views.friendscreens.FriendListState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class MainViewModel(

    private val repo: FriendRepo
): ViewModel() {
    private val TAG = "MainViewModel"

    private val _friends: MutableStateFlow<FriendListState> =
        MutableStateFlow(FriendListState())
    val friendsState :StateFlow<FriendListState> get() = _friends

    fun getFriends() = viewModelScope.launch{
        _friends.update {it.copy(isLoading = true)}
        val friends = repo.getFriends()

        when(friends) {
            is NetworkResponses.Error -> {
                _friends.update {
                    it.copy(
                        isLoading = false,
                        error = friends.message)
                }
            }
            is NetworkResponses.Loading -> {
                _friends.update {it.copy(isLoading = true)}
            }
            is NetworkResponses.SuccessfulFriendReq -> {
                _friends.update { it.copy(isLoading = false, friends = friends.friends) }
            } else -> {
            Log.e(TAG, "Ain't feelin well boss")
        }
        }
    }

    fun toggle(id:Int?){
        if (id != null){
        _friends.update { it.copy(toggle = true, selectedID = id) }
        } else {
            _friends.update { it.copy(toggle = false, selectedID = null) }
        }

    }
}

class VMLFactory (
    private val repo: FriendRepo
): ViewModelProvider.Factory {
    override fun<T : ViewModel> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(MainViewModel::class.java)){
            return MainViewModel(repo) as T
        } else {
            throw java.lang.IllegalArgumentException("whatchu talkn bout skeetr")
        }
    }
}


