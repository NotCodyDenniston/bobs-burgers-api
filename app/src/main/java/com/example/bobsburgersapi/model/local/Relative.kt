package com.example.bobsburgersapi.model.local

data class Relative (
    val name: String?,
    val wikiUrl: String?,
    val relationship: String?,
    val url: String?
)