package com.example.bobsburgersapi.model.mapper

import com.example.bobsburgersapi.model.local.Relative
import com.example.bobsburgersapi.model.remote.dto.RelativeDTO

class RelativeMapper:Mapper<RelativeDTO, Relative>{
    override fun invoke(dto: RelativeDTO): Relative {
       return Relative(
            dto.name,
        dto.wikiUrl,
        dto.relationship,
        dto.url,
        )
    }

}