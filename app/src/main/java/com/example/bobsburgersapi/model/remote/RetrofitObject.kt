package com.example.bobsburgersapi.model.remote

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

object RetrofitObject {

    private const val BASE_URL = "https://bobsburgers-api.herokuapp.com/"

   // private val mediaType = "application/json".toMediaType()

    val retrofit : Retrofit = Retrofit
        .Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    fun getFriendApiService(): FriendApiService = retrofit.create()
}