package com.example.bobsburgersapi.model.mapper

import com.example.bobsburgersapi.model.local.Friend
import com.example.bobsburgersapi.model.remote.dto.FriendsDTO
import com.example.bobsburgersapi.model.remote.dto.RelativeDTO

class FriendMapper:Mapper<FriendsDTO, Friend> {
    override fun invoke(dto: FriendsDTO): Friend {
        return Friend(
            dto.age,
            dto.firstEpisode,
            dto.gender,
            dto.hairColor,
            dto.id,
            dto.image,
            dto.name,
            dto.occupation,
            dto.relatives,
            dto.url,
            dto.voicedBy,
            dto.wikiUrl
        )
    }
}