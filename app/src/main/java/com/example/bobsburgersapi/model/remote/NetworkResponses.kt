package com.example.bobsburgersapi.model.remote

import com.example.bobsburgersapi.model.local.Friend

sealed class NetworkResponses<T> {

    data class SuccessfulFriendReq(
        val friends: List<Friend>
    ): NetworkResponses<List<Friend>>()

    object Loading:NetworkResponses<Unit>()

    data class Error(
        val message: String
    ): NetworkResponses<String>()
}