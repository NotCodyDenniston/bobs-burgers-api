package com.example.bobsburgersapi.model.remote.dto

@kotlinx.serialization.Serializable
data class RelativeDTO(
    val name: String?,
    val wikiUrl: String?,
    val relationship: String?,
    val url: String?
)