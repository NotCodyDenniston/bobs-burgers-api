package com.example.bobsburgersapi.model

import com.example.bobsburgersapi.model.local.Friend
import com.example.bobsburgersapi.model.mapper.FriendMapper
import com.example.bobsburgersapi.model.remote.FriendApiService
import com.example.bobsburgersapi.model.remote.NetworkResponses
import com.example.bobsburgersapi.model.remote.dto.FriendsResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

class FriendRepo(
    private val service: FriendApiService,
    private val mapper: FriendMapper
) {

    val numOfChars = "30"

    suspend fun getFriends(): NetworkResponses<*> = withContext(Dispatchers.IO) {
        val friendListResponse: Response<FriendsResponse> =
            service.getFriends(numOfChars).execute()

        return@withContext if (friendListResponse.isSuccessful) {
            val friendResponse = friendListResponse.body() ?: FriendsResponse()


            val friendList: List<Friend> = friendResponse.map {
                mapper(it)
            }
            NetworkResponses.SuccessfulFriendReq(friendList)
        } else {
            NetworkResponses.Error(friendListResponse.message())
        }
    }
}