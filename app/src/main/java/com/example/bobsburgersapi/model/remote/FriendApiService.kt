package com.example.bobsburgersapi.model.remote

import com.example.bobsburgersapi.model.remote.dto.FriendsResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface FriendApiService {

    @GET(CATEGORY_ENDPOINT)
    fun getFriends(@Query("limit")limit:String): retrofit2.Call<FriendsResponse>

    companion object{
    private const val CATEGORY_ENDPOINT="characters"

    }
}