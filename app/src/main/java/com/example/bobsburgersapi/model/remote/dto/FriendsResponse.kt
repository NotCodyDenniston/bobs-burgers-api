package com.example.bobsburgersapi.model.remote.dto

@kotlinx.serialization.Serializable
 class FriendsResponse: ArrayList<FriendsDTO>()