package com.example.bobsburgersapi.views.friendscreens

import com.example.bobsburgersapi.model.local.Friend

data class FriendListState (
    val isLoading: Boolean = false,
    val error: String = "",
    val friends: List<Friend> = emptyList(),
    val toggle: Boolean = false,
    val selectedID: Int? = null

)