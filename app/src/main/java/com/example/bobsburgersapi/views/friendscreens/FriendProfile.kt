package com.example.bobsburgersapi.views.friendscreens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import coil.compose.AsyncImage
import com.example.bobsburgersapi.model.local.Friend
import com.example.bobsburgersapi.model.remote.dto.RelativeDTO


@Composable
fun FriendProfile(
state: FriendListState,
toggle: () -> Unit
){
    Column(Modifier.clickable ( onClick = { toggle() } )) {
    val filteredFriend = state.friends.filter{ friend ->
        state.selectedID == friend.id
    }[0]
    AsyncImage(model = filteredFriend.image, contentDescription = null)
    Text(text = "NAME: ${filteredFriend.name}")
    Text(text = "AGE: ${filteredFriend.age}")
    Text(text = "GENDER: ${filteredFriend.gender}")
    Text(text = "HAIRCOLOR: ${filteredFriend.hairColor}")
    Text(text = "FIRST EPISODE: ${filteredFriend.firstEpisode}")
    Text(text = "OCCUPATION: ${filteredFriend.occupation}")
    Text(text = "VOICEACTOR: ${filteredFriend.voicedBy}")
    Text(text = "URL: ${filteredFriend.url}")
    Text(text = "WIKIURL: ${filteredFriend.wikiUrl}")
    Text(text = "RELATIVES:")
        filteredFriend.relatives?.forEach { relative ->
            Text(text = "Name: ${relative?.name}")
            Text(text = "Relationship: ${relative?.relationship}")
            Text(text = "WikiURL: ${relative?.wikiUrl}")
            Text(text = "URL: ${relative?.url}")
        }
    }
}
//val name: String?,
//val wikiUrl: String?,
//val relationship: String?,
//val url: String?
//
//val age: String?,
//val firstEpisode: String?,
//val gender: String?,
//val hairColor: String?,
//val id: Int?,
//val image: String?,
//val name: String?,
//val occupation: String?,
//val relatives: List<RelativeDTO?>?,
//val url: String?,
//val voicedBy: String?,
//val wikiUrl: String?

