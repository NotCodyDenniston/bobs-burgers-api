package com.example.bobsburgersapi.views.friendscreens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage

@Composable
fun FriendList(
    state: FriendListState,
    toggle: (Int?) -> Unit
) {
    LazyColumn() {
        state.friends.chunked(3).forEach {
            item {
                Row() {
                    it.forEach {
                        Column(
                            Modifier
                                .weight(.1F)
                                .clickable(onClick = {toggle(it.id) } )) {
                            AsyncImage(
                                model = it.image,
                                contentDescription = null,
                                Modifier.size(100.dp)
                            )
                            Text(text = "hello")

                        }

                    }
                }
            }
        }
    }
}