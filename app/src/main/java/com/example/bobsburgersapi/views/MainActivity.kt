package com.example.bobsburgersapi.views

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.example.bobsburgersapi.model.FriendRepo
import com.example.bobsburgersapi.model.mapper.FriendMapper
import com.example.bobsburgersapi.model.remote.RetrofitObject
import com.example.bobsburgersapi.ui.theme.BobsBurgersAPITheme
import com.example.bobsburgersapi.viewmodel.MainViewModel
import com.example.bobsburgersapi.viewmodel.VMLFactory
import com.example.bobsburgersapi.views.friendscreens.FriendList
import com.example.bobsburgersapi.views.friendscreens.FriendProfile

class MainActivity : ComponentActivity() {

    private val mainViewModel by viewModels<MainViewModel>{
        val service = RetrofitObject.getFriendApiService()
        val repo = FriendRepo( service ,FriendMapper())
        VMLFactory(repo)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainViewModel.getFriends()
        setContent {
            val friendListState by mainViewModel.friendsState.collectAsState()
            BobsBurgersAPITheme {
                // A surface container using the 'background' color from the theme
                Box(
                    modifier = Modifier.fillMaxSize(),
                ) {
                    if(!friendListState.toggle){
                        FriendList(state = friendListState, { id: Int? -> mainViewModel.toggle(id)})
                    } else {
                        FriendProfile(state = friendListState, { mainViewModel.toggle(null) })
                    }
                    if(friendListState.isLoading) {
                        CircularProgressIndicator(Modifier.align(Alignment.Center))
                    }
                }
            }
        }
    }
}






